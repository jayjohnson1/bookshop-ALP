using { sap.capire.bookshop as my } from '../db/schema';
service CatalogService @(path:'/browse') {

  /** For displaying lists of Books */
  @Aggregation.ApplySupported.PropertyRestrictions : true
  @readonly entity ListOfBooks as projection on Books {
    // @Analytics.Dimension
    // key ID,
    @Analytics.Dimension
    title,
        @Analytics.Dimension : true
    key author,
        @Analytics.Dimension : true
     genre,
        @Analytics.Measure   : true
        @Aggregation.default : #SUM
    stock,
        @Analytics.Measure   : true
        @Aggregation.default : #SUM
     price
  }
  // excluding { descr }
  actions { 
    action testActionInline (number : String @Common.Label : 'Line action:');
    action testActionNotInline (number : String @Common.Label : 'Table action:');
  };

  /** For display in details pages */
  @readonly entity Books as projection on my.Books { *,
    author.name as author
  } excluding { createdBy, modifiedBy }
  actions { 
    action bookActionInline (number : String @Common.Label : 'Line action:');
    action bookActionNotInline (number : String @Common.Label : 'Table action:');
  };

  @requires: 'authenticated-user'
  action submitOrder ( book: Books:ID, amount: Integer ) returns { stock: Integer };
  event OrderedBook : { book: Books:ID; amount: Integer; buyer: String };
}
