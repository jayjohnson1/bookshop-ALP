using CatalogService as service from '../../srv/cat-service';

annotate service.Books with @UI : {LineItem : [
    {
        $Type : 'UI.DataField',
        Value : author,
        Label : 'Author',
    },
    {
        $Type : 'UI.DataField',
        Value : price,
        Label : 'Price'
    },
    {
        $Type : 'UI.DataField',
        Value : title,
        Label : 'Title',
    },
    {
        $Type : 'UI.DataFieldForAction',
        Action : 'CatalogService.bookActionInline',
        // InvocationGrouping : ,
        Inline : true,
        // Determining : ,
        Label : 'Book action',
        // Criticality : ,
        // CriticalityRepresentation : ,
        // IconUrl : '',
    }
], };
