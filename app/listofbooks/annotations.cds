using CatalogService as service from '../../srv/cat-service';

annotate service.ListOfBooks with @UI : {LineItem : [
    {
        $Type : 'UI.DataField',
        Value : author,
        Label : 'Author',
    },
    {
        $Type : 'UI.DataField',
        Value : price,
        Label : 'Price'
    },
    // {
    //     $Type : 'UI.DataField',
    //     Value : title,
    //     Label : 'Title',
    // },
    {
        $Type              : 'UI.DataFieldForAction',
        Action             : 'CatalogService/ListOfBooks_testActionInline',
        // InvocationGrouping : #Isolated,
        Label              : '{i18n>Inline Action}',
        Inline : true,
        // Determining : true,
    },
    // {
    //     $Type              : 'UI.DataFieldForAction',
    //     Action             : 'CatalogService.EntityContainer/ListOfBooks_testActionInline',
    //     // InvocationGrouping : #Isolated,
    //     Label              : '{i18n>Not Inline Action}',
    //     Inline : false,
    //     // Determining : true,
    // }
], 
// Identification  : [{
//         //Determining : true,
//         $Type              : 'UI.DataFieldForAction',
//         Action             : 'CatalogService.ListOfBooks/ListOfBooks_testActionInline',
//         // InvocationGrouping : #Isolated,
//         Label              : '{i18n>Inline Action}',
//         // Inline : true,
// }],



        HeaderInfo               : {
            $Type          : 'UI.HeaderInfoType',
            TypeName       : '{i18n>Book}',
            TypeNamePlural : '{i18n>Books}',
            Title          : {Value : author},
            Description    : {Value : genre_ID}
        },
        // HeaderFacets             : [
        //     {
        //         $Type  : 'UI.ReferenceFacet',
        //         Target : '@UI.FieldGroup#attributes_A',
        //         Label  : '{i18n>_Attributes}',
        //     }
        // ],
        FieldGroup #attributes_A : {
            Label: 'Books',
            Data  : [
                {Value : price},
                {Value : title},
                {Value : stock},
            ]
        },
        Facets                   : [
            {
                $Type  : 'UI.CollectionFacet',
                Label  : 'Approval',
                Facets : [{
                    $Type  : 'UI.ReferenceFacet',
                    Label  : 'Approval',
                    Target : '@UI.FieldGroup#attributes_A'
                }, ]
            }
        ],
};
