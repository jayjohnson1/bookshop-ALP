sap.ui.define(
    ["sap/suite/ui/generic/template/lib/AppComponent"],
    function (Component) {
        "use strict";

        return Component.extend("listofbooks.Component", {
            metadata: {
                manifest: "json"
            }
        });
    }
);