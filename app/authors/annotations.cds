using AdminService as service from '../../srv/admin-service';

annotate service.Authors with @(UI.LineItem : [{
    $Type : 'UI.DataField',
    Value : name
}]);

annotate service.Authors with @(UI : {Facets : [{
    $Type  : 'UI.ReferenceFacet',
    Label  : '{i18n>_Books}',
    Target : 'books/@UI.PresentationVariant',
}]});

annotate service.Books with @(UI : {
    PresentationVariant : {
        $Type          : 'UI.PresentationVariantType',
        Visualizations : ['@UI.LineItem'],
        SortOrder      : [{
            $Type      : 'Common.SortOrderType',
            Property   : price,
            Descending : true
        }]
    },
    Identification      : [{Value : title}],
    LineItem            : [
        {Value : ID},
        {Value : title},
        {Value : price},
    ]
});
